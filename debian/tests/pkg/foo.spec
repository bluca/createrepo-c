%global _summary Demonstrate building an RPM package

%global _description \
The foo package demonstrates the process of building a source and \
binary RPM package (or more than one) from a spec file and a source \
archive.

%global debug_package %{nil}

Name:		foo
Summary:	%{_summary}
Version:	0.1.0
Release:	1%{?dist}
URL:		https://example.com/foo/

Source0:	https://files.example.com/foo/foo-0.1.0.tar.gz
License:	Public

Requires:	bc

%description %{_description}

The main package or something like that.

%package -n foo-common
Summary:	%{_summary} - common files

BuildArch:	noarch

Requires:	perl

%description -n foo-common %{_description}

Common files for all architectures.

%prep
%autosetup

%build
make %{?_smp_mflags} DEBUGINFOD_URLS=https://debuginfod.example.com

%install
%make_install

%files -n foo
%{_bindir}/foo

%files -n foo-common
%{_bindir}/foosetup

%changelog
* Thu Jun 27 2024 Peter Pentchev <roam@ringlet.net> - 0.1.0-2
- Mock DEBUGINFOD_URLS in case this test program is built with ELF package notes support.

* Sun May 24 2020 Peter Pentchev <roam@ringlet.net> - 0.1.0-1
- Initial version of a sample package.
